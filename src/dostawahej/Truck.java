/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dostawahej;

/**
 *
 * @author PM
 */
public class Truck {
    private int MaxCargo;
    private int Location;
    private String[] CargoIDs;
    
    public Truck(int MaxCargo, int Location){
        this.MaxCargo=MaxCargo;
        this.Location=Location;
        CargoIDs = new String[MaxCargo];
    }
    
    public int getLocation(){
        return Location;
    }
    
    public String[] getCargo(){
        return CargoIDs;
    }
}
