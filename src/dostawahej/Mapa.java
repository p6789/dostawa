/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dostawahej;

import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

/**
 *
 * @author PM
 */
public class Mapa {
int flag=0;
private String plik;
private int[][] tab;    

    public Mapa (String plik) throws IOException{
        this.plik = plik;
        createMap(plik);
    }

  private void createMap(String filePath) throws IOException {
      
  FileReader fileReader = new FileReader(filePath);
  BufferedReader bufferedReader = new BufferedReader(fileReader);
  int x,y,z;
  String textLine = bufferedReader.readLine();
  int n = Integer.parseInt(textLine.substring(0,1));
  tab = new int[n][n];
  
  do {
    if(!textLine.substring(0,1).equals("#") && flag>1) { 
    /*
    System.out.print(textLine.substring(0,1) + " ");
    System.out.print(textLine.substring(2,3) + " ");
    System.out.println(textLine.substring(4,textLine.length()));
    */
    x = Integer.parseInt(textLine.substring(0,1));
    y = Integer.parseInt(textLine.substring(2,3));
    z = Integer.parseInt(textLine.substring(4,textLine.length()));
    tab[x][y]=z;
    tab[y][x]=z;
    }
    else if (textLine.substring(0,1).equals("#")) flag++;
    
    textLine = bufferedReader.readLine();
  } while(textLine != null);
  //drawMap(tab,n);
  bufferedReader.close();
  }
    
  private void drawMap(int[][] tab, int n){
      int i,j;
      for (i=0;i<n;i++){
          for (j=0;j<n;j++){
              System.out.print(tab[i][j]+" ");
          }
          System.out.println();
  }
  }
}
