/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dostawahej;

import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

/**
 *
 * @author PM
 */
public class Orders {
    private int start;
    private String plik;
    private Cargo[] tab;
    
    public Orders (String plik) throws IOException{
    this.plik = plik;
    createCargoList(plik);
    }
    
    private void createCargoList(String filePath) throws IOException{
    FileReader fileReader = new FileReader(filePath);
    BufferedReader bufferedReader = new BufferedReader(fileReader);
    String textLine = bufferedReader.readLine();
    start = Integer.parseInt(textLine.substring(0,1));
    int n = Integer.parseInt(textLine.substring(2,3));
    tab = new Cargo[n];
    textLine = bufferedReader.readLine();
    
    for(int i=0;i<n;i++){
      int pr=0;
      String comm="";
      String id = textLine.substring(0,3);
      int dst = Integer.parseInt(textLine.substring(6,7));
      
      if (textLine.substring(textLine.length()-2,textLine.length()-1).equals(" ")){
          pr = Integer.parseInt(textLine.substring(textLine.length()-1,textLine.length()));
          comm = textLine.substring(8,textLine.length()-2);
      }
      else if (textLine.substring(textLine.length()-3,textLine.length()-2).equals(" ")){
          pr = Integer.parseInt(textLine.substring(textLine.length()-2,textLine.length()));
          comm = textLine.substring(8,textLine.length()-3);
      }
      else if (textLine.substring(textLine.length()-4,textLine.length()-3).equals(" ")){
          pr = Integer.parseInt(textLine.substring(textLine.length()-3,textLine.length()));
          comm = textLine.substring(8,textLine.length()-4);
      }
      
      tab[i]=new Cargo(id,start,dst,comm,pr);
      textLine = bufferedReader.readLine();
    }
    //printOrders(tab,n);
    sortOrders(tab,n);
    printOrders(tab,n);
    }
    
    private void printOrders(Cargo[] tab, int n){
        for (int i=0;i<n;i++){
            tab[i].printCargo();
        }
    }
    
    private void sortOrders(Cargo[] tab, int n){
        int flag=1;
        Cargo[] tmp = new Cargo[1];
        while (flag==1){
            flag=0;
        for (int i=1;i<n;i++){
            if (tab[i-1].getPriority()<tab[i].getPriority()){
            tmp[0]=tab[i];
            tab[i]=tab[i-1];
            tab[i-1]=tmp[0];
            flag=1;
            }
        }
        }
    }
    
    public int getStart(){
        return start;
    }
}
