/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dostawahej;

/**
 *
 * @author PM
 */
public class Cargo {
    private String id;
    private int source;
    private int destination;
    private String comment;
    private int priority;
    
    public Cargo(String id, int source, int destination, String comment, int priority){
        this.id=id;
        this.source=source;
        this.destination=destination;
        this.comment=comment;
        this.priority=priority;
    }    
    
    public void printCargo(){
        System.out.println(id + " " + source + " " + destination + " " + comment + " " + priority);
    }
    
    public int getPriority(){
        return priority;
    }
    
}
